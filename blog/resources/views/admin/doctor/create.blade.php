@extends('admin.layouts.master')


       @section('form')
           <section class="content">

               <div class="row">
                   <div class="col-md-3">

                       <!-- Profile Image -->
                       <div class="box box-primary">
                           <div class="box-body box-profile">
                               <img class="profile-user-img img-responsive img-circle" src="{{asset('../../back-end/dist/img/dc.jpg')}}" alt="User profile picture">

                               <h3 class="profile-username text-center">Alexander Nazem</h3>

                               <p class="text-muted text-center">Doctor Profile</p>

                               <ul class="list-group list-group-unbordered">
                                   <li class="list-group-item">
                                       <b>Name</b> <a class="pull-right">Alexander Nazem</a>
                                   </li>
                                   <li class="list-group-item">
                                       <b>Specialist</b> <a class="pull-right">Urologist</a>
                                   </li>
                                   <li class="list-group-item">
                                       <b>Experience</b> <a class="pull-right">2 Years</a>
                                   </li>
                               </ul>

                               <a href="#" class="btn btn-primary btn-block"><b>Update Profile</b></a>
                           </div>
                           <!-- /.box-body -->
                       </div>
                       <!-- /.box -->

                       <!-- About Me Box -->
                       <div class="box box-primary">
                           <div class="box-header with-border">
                               <h3 class="box-title">About Me</h3>
                           </div>
                           <!-- /.box-header -->
                           <div class="box-body">
                               <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

                               <p class="text-muted">
                                   B.S. in Computer Science from the University of Tennessee at Knoxville
                               </p>

                               <hr>

                               <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

                               <p class="text-muted">Malibu, California</p>

                               <hr>

                               <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

                               <p>
                                   <span class="label label-danger">UI Design</span>
                                   <span class="label label-success">Coding</span>
                                   <span class="label label-info">Javascript</span>
                                   <span class="label label-warning">PHP</span>
                                   <span class="label label-primary">Node.js</span>
                               </p>

                               <hr>

                               <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                           </div>
                           <!-- /.box-body -->
                       </div>
                       <!-- /.box -->
                   </div>

                   <div class="col-md-9">
                       <div class="nav-tabs-custom">
                           <ul class="nav nav-tabs">
                               <li class="active"><a href="#activity" data-toggle="tab">Add Doctor</a></li>

                           </ul>
                           <div class="tab-content">
                               <div class="active tab-pane" id="activity">
                                   <!-- Post -->
                                   <div class="post">
                                       <form class="form-horizontal">
                                           <div class="form-group">
                                               <label for="inputName" class="col-sm-2 control-label">Name</label>

                                               <div class="col-sm-10">
                                                   <input type="email" class="form-control" id="inputName" placeholder="Name">
                                               </div>
                                           </div>




                                           <div class="form-group">
                                               <label for="" class="col-sm-2 control-label">Specialist</label>
                                               <div class="col-sm-10">
                                                   <select class="form-control" id="sel1">
                                                       <option value="select">Select </option>
                                                       <option>Dermatologist </option>
                                                       <option>Cardiologist</option>
                                                       <option>Gastroenterologist </option>
                                                       <option>Neurologist </option>
                                                       <option>Obstetrician  </option>
                                                       <option>Gynecologist  </option>
                                                       <option>Pathologist  </option>
                                                       <option>Pediatrician  </option>
                                                       <option>Podiatrist   </option>
                                                       <option>Urologist   </option>
                                                       <option>Rheumatologist  </option>
                                                   </select>
                                               </div>
                                           </div>
                                           <div class="form-group">
                                               <label for="" class="col-sm-2 control-label">Department</label>
                                               <div class="col-sm-10">
                                                   <select class="form-control" id="sel1">
                                                       <option value="select">Select </option>
                                                       <option>Accident and emergency (A&E) </option>
                                                       <option>Anaesthetics</option>
                                                       <option>Breast screening </option>
                                                       <option>Cardiology </option>
                                                       <option>Chaplaincy  </option>
                                                       <option>Critical care  </option>
                                                       <option>Diagnostic imaging  </option>
                                                       <option>Discharge lounge  </option>
                                                   </select>
                                               </div>
                                           </div>
                                           <div class="form-group">
                                               <label for="inputName" class="col-sm-2 control-label">Medical Name</label>

                                               <div class="col-sm-10">
                                                   <input type="text" class="form-control" id="name" placeholder="Medical Name">
                                               </div>
                                           </div>
                                           <div class="form-group">
                                               <label for="inputExperience" class="col-sm-2 control-label">Experience</label>

                                               <div class="col-sm-10">
                                                   <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                                               </div>
                                           </div>
                                           <div class="form-group">
                                               <label for="inputExperience" class="col-sm-2 control-label">Medical Location</label>

                                               <div class="col-sm-10">
                                                   <input type="text" class="form-control" id="name" placeholder="Search location">
                                               </div>
                                           </div>

                                           <div class="form-group">
                                               <div class="col-sm-offset-2 col-sm-10">
                                                   <div class="checkbox">
                                                       <label>
                                                           <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                                       </label>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="form-group">
                                               <div class="col-sm-offset-2 col-sm-10">
                                                   <button type="submit" class="btn btn-danger">Submit</button>
                                               </div>
                                           </div>
                                       </form>

                                   </div>

                               </div>

                           </div>
                           <!-- /.tab-content -->
                       </div>
                       <!-- /.nav-tabs-custom -->
                   </div>

               </div>


           </section>
        @endsection

